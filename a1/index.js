// https://jsonplaceholder.typicode.com/todos

// create a Fetch request using GET that will retrieve all the to do list item
async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos')
	let json = await result.json()

	let arr = []

	arr.push(json)
// using map method return only the title of every item and print the result in console
	const titleFilter = json.map((data) => data.title);
	console.log(titleFilter)
}
	
	fetchData();


// Create a fetch request using GET method that will retrieve a single to do list item	
async function fetchData2(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos/12')
	let json = await result.json()

// Using the data retrieved , print a message in the console that will provide the title and status of the to do list
	console.log(`The title is : ${json.title} and the status is ${json.completed}`)
}
	
	fetchData2();


// Create a fetch request using POST method that will create a to do list using JSON placeholder API
fetch('https://jsonplaceholder.typicode.com/todos', {
				method : 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body : JSON.stringify({
					completed: true,
					title: 'New to do item',
					userId: 20
				})

			})
			.then((response) => response.json())
			.then((json) => console.log(json))


// Create a fetch request using PUT method that will update a to do list using JSON placeholder API

fetch('https://jsonplaceholder.typicode.com/todos/27',{
				method : 'PUT',
				headers: {
					'Content-Type': 'application/json'
				},
				body : JSON.stringify({
					complete : false,
					title : 'Updated to do item',
					userId : 18
				})
			})
			.then((response)=> response.json())
			.then((json) => console.log(json))

// Update a to do list item by changing the structure to contain the following properties
/*
	a. Title
	b. Description
	c. Status
	d. Data Completed
	e. UserId
*/
fetch('https://jsonplaceholder.typicode.com/todos/30',{
				method : 'PUT',
				headers: {
					'Content-Type': 'application/json'
				},
				body : JSON.stringify({
					title : 'Hello world',
					description : 'to do nothing',
					status : false,
					dataCompleted : 'October,26,2021',
					userId: 20
				})
			})
			.then((response)=> response.json())
			.then((json) => console.log(json))

// Create a fetch request using the PATCH that will update a to do list item using JSON placeholder API

	fetch('https://jsonplaceholder.typicode.com/todos/21' , {
				method : 'PATCH',
				headers : {
					'Content-Type' : 'application/json'
				},
				body : JSON.stringify({
					title: 'New title'
				})
			})
			.then((response)=> response.json())
			.then((json) => console.log(json))


// Update a to do list item by changing the status to complete and add date when the status was changed

fetch('https://jsonplaceholder.typicode.com/todos/12',{
				method : 'PUT',
				headers: {
					'Content-Type': 'application/json'
				},
				body : JSON.stringify({
					status : true,
					dateChange: 'October,26,2021'
				})
			})
			.then((response)=> response.json())
			.then((json) => console.log(json))


// Create a request via Postman to retrieve all the to do list items
