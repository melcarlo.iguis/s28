// Synchronous and Asynchronous 

	// Javascript is by default synchronous 
		// it only runs one statement at a time.

console.log('Hello World')

console.log('Hello Again')


// for(let i = 0 ; i<=1500 ; i++ ){

// 	console.log(i)
// }

// console.log('The end')

	// When an action will take some time to process, this results in code "blocking"

// Asynchronous - that we can proceed to execute other statements while time consuming code is running the background.

// Getting all post

	/*
		Syntax 
			fetch('URL')
			console.log(fetch())


	*/


		// fetch('https://jsonplaceholder.typicode.com')
		// 	console.log(fetch('https://jsonplaceholder.typicode.com'))


		/*
			Syntax:
			fetch('URL')
			.then(response) => {})
		*/


		// fetch('https://jsonplaceholder.typicode.com/posts')
		// 	.then((response) => console.log(response.status))


			// .then - captures the response object and return another promise which will eventually be resolved or rejected.


		// fetch('https://jsonplaceholder.typicode.com/posts')
		// 	.then((response) => response.json())

			// json method from response object to convert the retrieve data into json format to be used to our application

			// .then((json) => console.log(json))

			// print the converted JSON value from the fetch request.

		// Using multiple ".then" methods creates a "promise chain"


		// Async - Await


		async function fetchData(){
			let result = await fetch('https://jsonplaceholder.typicode.com/posts')
			console.log(result)
			console.log(typeof result)
			console.log(result.body);

			let json = await result.json()

			console.log(json)
		}

		// fetchData();

		// Syntactic Sugar - 

			// makes the code more readable

			// await is syntactic sugar of .then


			// Retrieve specific post

			// fetch('https://jsonplaceholder.typicode.com/posts/14')
			// .then((response) => response.json())
			// .then((json) => console.log(json))

		// Creating a post

		/*

			fetch('URL', options)
				.then(response) => {})
				.then(response) => {})

		*/

			// fetch('https://jsonplaceholder.typicode.com/posts', {
			// 	method : 'POST',
			// 	headers: {
			// 		'Content-Type' : 'application/json'
			// 	},
			// 	body : JSON.stringify({
			// 		title: 'New Post',
			// 		body: 'Hello World',
			// 		userId: 1
			// 	})

			// })
			// .then((response) => response.json())
			// .then((json) => console.log(json))


// Update

			// fetch('https://jsonplaceholder.typicode.com/posts/14',{
			// 	method : 'PUT',
			// 	headers: {
			// 		'Content-Type': 'application/json'
			// 	},
			// 	body : JSON.stringify({
			// 		title : 'Updated Post',
			// 		body : 'Hello once more',
			// 		userId : 1
			// 	})
			// })
			// .then((response)=> response.json())
			// .then((json) => console.log(json))


		// updating post with PATCH

			// fetch('https://jsonplaceholder.typicode.com/posts/1' , {
			// 	method : 'PATCH',
			// 	headers : {
			// 		'Content-Type' : 'application/json'
			// 	},
			// 	body : JSON.stringify({
			// 		title: 'Corrected Post'
			// 	})
			// })
			// .then((response)=> response.json())
			// .then((json) => console.log(json))
	
	// PUT and PATCH

		// both deals with updates
		// the number of properties being charged.

	// DELETING A POST

		// fetch('https://jsonplaceholder.typicode.com/posts/1' , {
		// 	method : 'DELETE'
		// })

		// fetch('https://jsonplaceholder.typicode.com/posts?userId=1' , {
		// 	method: 'DELETE'
		// })
		// .then((response) => response.json())
		// .then((json)=> console.log(json))


		// Data can be filtered by sending the userId along with the URL
		// Information sent via the URL can be done by adding question mark (?)

		
